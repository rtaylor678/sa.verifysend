﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Configuration;
using System.Xml;

namespace DBDownload
{
    public class DBDownload
    {
        private byte[] Key = { 248, 215, 80, 175, 235, 183, 19, 200, 195, 97, 212, 123, 110, 0, 192, 83, 148, 33, 49, 121, 147, 240, 138, 188, 113, 177, 209, 112, 145, 219, 53, 78 };
        private byte[] Vector = { 86, 102, 197, 82, 75, 101, 134, 69, 250, 94, 79, 187, 146, 83, 18, 128 };
        private string directory = ConfigurationManager.AppSettings["Directory"].ToString();
        private string filename = ConfigurationManager.AppSettings["Filename"].ToString();

        static void Main(string[] args)
        {
            DBDownload dbdl = new DBDownload();
            dbdl.saveDataLocal();
        }

        #region Encrypt Text
        public byte[] encrypt(string TextValue)
        {
            using (RijndaelManaged rm = new RijndaelManaged())
            {
                ICryptoTransform EncryptorTransform = rm.CreateEncryptor(Key, Vector);
                
                UTF8Encoding UTFEncoder = new UTF8Encoding();
                Byte[] unencryptedValue = UTFEncoder.GetBytes(TextValue);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, EncryptorTransform, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(unencryptedValue, 0, unencryptedValue.Length);
                        cryptoStream.FlushFinalBlock();

                        memoryStream.Position = 0;
                        byte[] encrypted = new byte[memoryStream.Length];
                        memoryStream.Read(encrypted, 0, encrypted.Length);

                        return encrypted;
                    }
                }
            }
        }
        #endregion

        #region Save Data Local
        /// <summary>
        /// Saves database table locally to .csv file
        /// </summary>
        private void saveDataLocal()
        {
            string query = @"SELECT COMPANYID, ALIAS, ALIASTYPE FROM GlobalDB.SAMail.Aliases";
            DataSet ds = new DataSet();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["QCDBConn"].ToString());
            using (conn)
            {
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                conn.Open();
                da.Fill(ds);
            }

            XmlDocument verifySendXML = new XmlDocument();
            verifySendXML.LoadXml(ds.GetXml());

            XDocument doc = XDocument.Parse(verifySendXML.InnerXml);
            StringBuilder sb = new StringBuilder();

            sb.Append("COMPANYID,ALIAS,ALIASTYPE");
            sb.AppendLine();
            foreach (XElement node in doc.Descendants("Table"))
            {
                foreach (XElement innerNode in node.Elements())
                {
                    sb.AppendFormat("{0},", innerNode.Value);
                }
                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();
            }

            //File.WriteAllText(directory + filename, encrypt(Encoding.Unicode.GetBytes(sb.ToString())));
            File.WriteAllText(directory + filename, Convert.ToBase64String(encrypt(sb.ToString())));
        }
        #endregion
    }
}
